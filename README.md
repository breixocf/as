# Resumen

Este repositorio contiene las prácticas de Arquitectura Software realizadas por `Breixo Camiña Fernández` para el curso 2014/2015.

# Estructura

El repositorio se estructura en un directorio por cada práctica.

Los ficheros de los enunciados siguen el formato markdow.
