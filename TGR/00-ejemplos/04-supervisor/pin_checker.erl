%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright 2014
%%% @doc Implementación dun comprobador de pins (usando gen_fsm).
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(pin_checker).
-behaviour(gen_fsm).

%% Public API
-export([iniciar/1,
	 pulsar_dixito/1,
	 deter/0]).
%% Internal API (gen_fsm)
-export([init/1, handle_event/3, terminate/3]).
%% Internal API (estados)
-export([agardando/2, introducindo_pin/2]).

%% Macro definition
-define(CHECKER, pinchecker).

%%--------------------------------------------------------------------
%% @doc Inicia o compr%obador.
%% @end
%% -------------------------------------------------------------------
%-spec iniciar(PIN :: [integer(), integer(), integer(), integer()]) -> ok.
iniciar(PIN) ->
    gen_fsm:start_link({local, ?CHECKER}, ?MODULE, PIN, []).

%%--------------------------------------------------------------------
%% @doc Introduce un díxito do PIN.
%% @end
%% -------------------------------------------------------------------
-spec pulsar_dixito(D :: integer()) -> ok.
pulsar_dixito(D) ->
    gen_fsm:send_event(?CHECKER, {dixito, D}).

%%--------------------------------------------------------------------
%% @doc Detén o comprobador.
%% @end
%% -------------------------------------------------------------------
-spec deter() -> ok.
deter() ->
    gen_fsm:send_all_state_event(?CHECKER, stop).



%% ===== ===== ===== ===== ===== ===== ===== ===== =====

init(PIN) ->
    {ok, agardando, {PIN, []}}.

agardando({dixito, D}, {[D | RestoPIN], []}) -> % first digit OK
    {next_state, introducindo_pin, {RestoPIN, [D]}};
agardando({dixito, _D}, {PIN, []}) -> 
    io:format(" *** ERROR! *** ~n"),
    {next_state, agardando, {PIN, []}}.

introducindo_pin({dixito, D}, {[D] , Acc}) -> % last digit OK
    io:format(" *** PIN OK! *** ~n"),
    {next_state, agardando, {Acc ++ [D], []}};
introducindo_pin({dixito, D}, {[D | RestoPIN], Acc}) -> % next digit OK, not last
    {next_state, introducindo_pin, {RestoPIN, Acc ++ [D]}};
introducindo_pin({dixito, D}, {PIN, Acc}) -> % next digit not OK
    io:format(" *** ERROR! (reset) *** ~n"),
    {stop, pin_error, {Acc ++ [D], PIN}}.

handle_event(stop, _Estado, _DatosEstado) ->
    {stop, normal, []}.

terminate(normal, _Estado, _DatosEstado) ->
    ok.
