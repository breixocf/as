%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright 2014
%%% @doc Exemplo de uso do behaviour OTP: supervisor.
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(supervisor_pin).
-behaviour(supervisor).

%% Public API
-export([start/1]).
%% Internal API (supervisor)
-export([init/1]).

%%--------------------------------------------------------------------
%% @doc Arrinca o supervisor.
%% @end
%% -------------------------------------------------------------------
start(PIN) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, [PIN]).



%% ===== ===== ===== ===== ===== ===== ===== ===== =====

init([PIN]) ->
    % estratexia de reinicio de procesos supervisados (one_for_one, one_for_all, rest_for_one)
    RestartStrategy = one_for_one,
    % máxima frecuencia de reinicio (10 reinicios cada 3600 segundos)
    MaxRestarts = 10,
    MaxSecondsBetweenRestarts = 3600,

    Flags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    % estratexia de reinicio (permanent: sempre; temporary: nunca; transient: se morre con 'exit')
    Restart = permanent,
    % tempo que permitimos agardar por un proceso supervisado para que execute o seu terminate/2
    Shutdown = 2000,
    % tipo de procesos que imos supervisar
    Type = worker,

    Checker = {pin_checker, {pin_checker, iniciar, [PIN]},
              Restart, Shutdown, Type, [pin_checker]},

    {ok, {Flags, [Checker]}}.
