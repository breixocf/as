%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright 2014
%%% @doc Implementación de log de eventos do proceso-sumador
%%%      (usando gen_event).
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(log_adder).
-behaviour(gen_event).

%% Internal API (gen_event)
-export([init/1, handle_event/2, terminate/2]).

%% ===== ===== ===== ===== ===== ===== ===== ===== =====
%@private
init(Ficheiro) ->
    {ok, Fd} = file:open(Ficheiro, [read, append]),
    {ok, Fd}.

%@private
handle_event({operacion, suma, Cantidade, Acumulado, NovoResultado}, Fd) ->
    Timestamp = format_utc_timestamp(),
    io:format(Fd, "[~s] Realizase a suma ~p + ~p con resultado ~p~n", [Timestamp, Cantidade, Acumulado, NovoResultado]),
    {ok, Fd}.

%@private
terminate([], Fd) ->
    file:close(Fd).

% Internal stuff %
%@private
% format function copied from Erlang 'os' module documentation
format_utc_timestamp() ->
    TS = {_,_,Micro} = os:timestamp(),
    {{Year,Month,Day},{Hour,Minute,Second}} =	calendar:now_to_universal_time(TS),
    Mstr = element(Month,{"Xan","Feb","Mar","Abr","Mai","Xun","Xul","Ago","Set","Out","Nov","Dec"}),
    io_lib:format("~2w ~s ~4w ~2w:~2..0w:~2..0w.~6..0w",
    [Day,Mstr,Year,Hour,Minute,Second,Micro]).