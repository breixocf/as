%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright 2013
%%% @doc Sinxelo servidor con estado para sumar (ficheiro de probas EUnit).
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(adder_tests).

-include_lib("eunit/include/eunit.hrl").

-define(TESTMODULE, gen_adder).

%%--------------------------------------------------------------------
%% @doc Meta-caso de proba que encadena varias operación nun mesmo
%%      escenario (interesante para compoñentes con estado).
%% @spec adder_test_() -> boolean()
%% @end
%% --------------------------------------------------------------------
adder_test_() ->
    {setup,
     fun()  -> ?TESTMODULE:start() end,  % preparar el entorno de pruebas para la ejecución
     fun(_) -> ?TESTMODULE:stop()  end,  % limpiar el entorno de pruebas tras la ejecución
     fun(_) ->
	     {inorder,
 	      [?_assertMatch({ok, 0}, ?TESTMODULE:add(0)),
 	       ?_assertMatch({ok, 1}, ?TESTMODULE:add(1)),
 	       ?_assertMatch({ok,11}, ?TESTMODULE:add(10)),
 	       ?_assertMatch({ok, 6}, ?TESTMODULE:add(-5)),
 	       ?_assertMatch({ok,-5}, ?TESTMODULE:add(-11)),
 	       ?_assertMatch({ok,-5}, ?TESTMODULE:add(0))]}
     end}.

