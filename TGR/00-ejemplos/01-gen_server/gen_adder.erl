%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright 2014
%%% @doc Implementación dun proceso-sumador (usando gen_server).
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(gen_adder).
-behaviour(gen_server).

%% Public API
-export([start/0, stop/0, add/1]).

%% Internal API (gen_server)
-export([init/1, handle_call/3, handle_cast/2, terminate/2, code_change/3, handle_info/2]).

%%--------------------------------------------------------------------
%% @doc Lanza o servidor (ou falla se xa existe).
%% @end
%% -------------------------------------------------------------------
-spec start() -> started.
start() ->
    {ok, _Pid} = gen_server:start({local, sumador}, ?MODULE, [], []),
    started.

%%--------------------------------------------------------------------
%% @doc Para o servidor (nunca falla, aínda que estivera arrincado).
%% @end
%% -------------------------------------------------------------------
-spec stop() -> ok.
stop() ->
    gen_server:cast(sumador, stop).

%%--------------------------------------------------------------------
%% @doc Manda un enteiro ao servidor para que acumule a cantidade.
%%      Devolve o resultado acumulado.
%% @end
%% -------------------------------------------------------------------
-spec add(N :: integer()) -> {ok, integer()}.
add(N) ->
    gen_server:call(sumador, {add, N}).



%% ===== ===== ===== ===== ===== ===== ===== ===== =====
% @private
init([]) ->
    {ok, 0}.

% @private
handle_cast(stop, AccTotal) ->
    {stop, normal, AccTotal}.

% @private
handle_call({add, N}, _From, AccTotal) ->
    {reply, {ok, N+AccTotal}, N+AccTotal}.

% @private
handle_info(_Info, _AccTotal) ->
    {noreply, _AccTotal}.

% @private
terminate(normal, _AccTotal) ->
    ok.

% @private
code_change(_Version, AccTotal, _Args) ->
    {ok, AccTotal}.
