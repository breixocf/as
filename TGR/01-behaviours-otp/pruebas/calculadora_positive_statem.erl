%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura M. Castro <lcastro@udc.es>
%%% @copyright 2015
%%% @doc Especificación de probas PROPER (máquina de estados finitos)
%%%      para a calculadora (tarefa do TGR1). Testing positivo.
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(calculadora_positive_statem).
-behaviour(proper_statem).

-include_lib("proper/include/proper.hrl").

%% CALLBACKS from proper_statem
-export([initial_state/0, command/1, precondition/2, postcondition/3, next_state/3]).
-export([suma/2, resta/2, producto/2, division/2]).
-export([off/0]).

%%--------------------------------------------------------------------
%% @doc Returns the state in which each test case starts.
%% @spec initial_state() -> proper_statem:symbolic_state()
%% @end
%%--------------------------------------------------------------------
initial_state() ->
    {stopped, suma, 0}.

%%--------------------------------------------------------------------
%% @doc Command generator, S is the current state.
%% @spec command(S :: proper_statem:symbolic_state())
%%                 -> proper_statem:symb_call()
%% @end
%%--------------------------------------------------------------------
command(_S) ->
    frequency([{50, {call, calculadora, on,  []}},
	       { 5, {call, ?MODULE, off, []}},
	       {25, {call, calculadora, acumular, [number()]}},
	       {20, {call, calculadora, modo, [modo()]}}]).

modo() ->
    elements([suma, resta, producto, division]).

%%--------------------------------------------------------------------
%% @doc Next state transformation, S is the current state. Returns
%%      next state.
%% @spec next_state(S :: proper_statem:symbolic_state(),
%%                  V :: proper_symb:var_id(), 
%%                  C :: proper_statem:symb_call())
%%                    -> proper_statem:symbolic_state()
%% @end
%%--------------------------------------------------------------------
next_state({stopped, _Op, _S}, _V, {call, calculadora, on,  []}) ->
    {started, suma, 0};
next_state({started, _Op, _S}, _V, {call, ?MODULE, off, []}) ->
    {stopped, suma, 0};
next_state({started,  Op,  S}, _V, {call, calculadora, acumular, [N]}) ->
    {started, Op, erlang:apply(?MODULE, Op, [S, N])};
next_state({started, _Op,  S}, _V, {call, calculadora, modo, [NewOp]}) ->
    {started, NewOp, S};
next_state(S, _V, {call, _, _, _}) ->
    S.

%%--------------------------------------------------------------------
%% @doc Precondition, checked before command is added to the command
%%      sequence. 
%% @spec precondition(S :: proper_statem:symbolic_state(),
%%                    C :: proper_statem:symb_call()) -> boolean().
%% @end
%%--------------------------------------------------------------------
precondition({stopped, _Op, _S}, {call, calculadora, on,  []}) ->
    true;
precondition({started, _Op, _S}, {call, ?MODULE, off, []}) ->
    true;
precondition({started, _Op, _S}, {call, calculadora, acumular, [_N]}) ->
    true;
precondition({started, _Op, _S}, {call, calculadora, modo, [_NewOp]}) ->
    true;
precondition(_S, {call, _, _, _}) ->
    false.

%%--------------------------------------------------------------------
%% @doc Postcondition, checked after command has been evaluated
%%      Note: S is the state before next_state(S,_,C) 
%% @spec postcondition(S :: proper_statem:symbolic_state(),
%%                     C :: proper_statem:call(), 
%%                     Res :: term()) -> boolean()
%% @end
%%--------------------------------------------------------------------
postcondition({stopped, _Op, _S}, {call, calculadora, on,  []}, {ok, _PID}) ->
    true;
postcondition({started, _Op, _S}, {call, ?MODULE, off, []}, ok) ->
    true;
postcondition({started, Op, S}, {call, calculadora, acumular, [N]}, Res) ->
    Res == ?MODULE:Op(S, N);
postcondition({started, _Op, _S}, {call, calculadora, modo, [_NewOp]}, ok) ->
    true;
postcondition(_S, {call, _, _, _}, _Res) ->
    false.



%%--------------------------------------------------------------------
%% @doc Default property
%% @spec prop_calculadora() -> proper:test()
%% @end
%%--------------------------------------------------------------------
prop_calculadora() ->
    ?FORALL(Cmds, commands(?MODULE),
	    begin
		{H, S, Res} = run_commands(?MODULE,Cmds),
		cleanup(),
		?WHENFAIL(io:format("History: ~p\nState: ~p\nRes: ~p\n", [H, S, Res]),
			  aggregate(command_names(Cmds), Res == ok))
	    end).



%%--------------------------------------------------------------------
%% Internal wrappers and auxiliary functions
%%--------------------------------------------------------------------
off() ->
    calculadora:off(),
    timer:sleep(100).

cleanup() ->
    catch off().

suma(A, B) ->
    A + B.
    
resta(A, B) ->
    A - B.
    
producto(A, B) ->
    A * B.

division(A, B) ->
    A / B.
