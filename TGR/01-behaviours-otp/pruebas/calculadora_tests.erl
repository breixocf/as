%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright 2015
%%% @doc Escenario de probas EUnit para a calculadora do TGR1.
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(calculadora_tests).

-include_lib("eunit/include/eunit.hrl").

-define(TESTMODULE, calculadora).

%%--------------------------------------------------------------------
%% @doc Meta-caso de proba que encadena varias operación nun mesmo
%%      escenario (interesante para compoñentes con estado).
%% @spec adder_test_() -> boolean()
%% @end
%% --------------------------------------------------------------------
adder_test_() ->
    {setup,
     fun()  -> ?TESTMODULE:on()   end,
     fun(_) -> ?TESTMODULE:off()  end,
     fun(_) ->
	     {inorder,
 	      [?_assertEqual(  3, ?TESTMODULE:acumular( 3)),
 	       ?_assertEqual(  1, ?TESTMODULE:acumular(-2)),
 	       ?_assertMatch( ok, ?TESTMODULE:modo(resta)),
 	       ?_assertEqual(  6, ?TESTMODULE:acumular(-5)),
 	       ?_assertEqual(  4, ?TESTMODULE:acumular( 2)),
 	       ?_assertMatch( ok, ?TESTMODULE:modo(division)),
 	       ?_assertEqual(1.0, ?TESTMODULE:acumular( 4)),
 	       ?_assertMatch( ok, ?TESTMODULE:modo(producto)),
 	       ?_assertEqual(7.0, ?TESTMODULE:acumular( 7))]}
     end}.

