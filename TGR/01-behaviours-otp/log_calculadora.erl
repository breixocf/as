%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Breixo Camiña Fernández
%%% @copyright (C) 2015
%%% @doc
%%%
%%% @end
%%% Created : 16. Mar 2015 7:55 PM
%%%-------------------------------------------------------------------
-module(log_calculadora).
-author("BreixoCF").

-behaviour(gen_event).

%% gen_event callbacks
-export([init/1, handle_event/2, terminate/2]).

%%%===================================================================
%%% gen_event callbacks
%%%===================================================================
init(Ficheiro) ->
    {ok, Fd} = file:open(Ficheiro, [read, append]),
    {ok, Fd}.

terminate(_Arg, Fd) ->
  file:close(Fd).

%%% Escritura Encendido
handle_event({on}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Calculadora ENCENDIDA~n", [Timestamp]),
  {ok, Fd};

%%% Escritura Apagado
handle_event({off}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Calculadora APAGADA~n", [Timestamp]),
  {ok, Fd};

%%% Escritura de Cambios de Modo
%%% Cambio SUMA -> ?
handle_event({cambio, suma, suma}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "SUMA", "SUMA"]),
  {ok, Fd};
handle_event({cambio, suma, resta}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "SUMA", "RESTA"]),
  {ok, Fd};
handle_event({cambio, suma, producto}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "SUMA", "PRODUCTO"]),
  {ok, Fd};
handle_event({cambio, suma, division}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "SUMA", "DIVISION"]),
  {ok, Fd};

%%% Cambio RESTA -> ?
handle_event({cambio, resta, suma}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "RESTA", "SUMA"]),
  {ok, Fd};
handle_event({cambio, resta, resta}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "RESTA", "RESTA"]),
  {ok, Fd};
handle_event({cambio, resta, producto}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "RESTA", "PRODUCTO"]),
  {ok, Fd};
handle_event({cambio, resta, division}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "RESTA", "DIVISION"]),
  {ok, Fd};

%%% Cambio PRODUCTO -> ?
handle_event({cambio, producto, suma}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "PRODUCTO", "SUMA"]),
  {ok, Fd};
handle_event({cambio, producto, resta}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "PRODUCTO", "RESTA"]),
  {ok, Fd};
handle_event({cambio, producto, producto}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "PRODUCTO", "PRODUCTO"]),
  {ok, Fd};
handle_event({cambio, producto, division}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "PRODUCTO", "DIVISION"]),
  {ok, Fd};

%%% Cambio DIVISION -> ?
handle_event({cambio, division, suma}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "DIVISION", "SUMA"]),
  {ok, Fd};
handle_event({cambio, division, resta}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "DIVISION", "RESTA"]),
  {ok, Fd};
handle_event({cambio, division, producto}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "DIVISION", "PRODUCTO"]),
  {ok, Fd};
handle_event({cambio, division, division}, Fd) ->
  Timestamp = format_utc_timestamp(),
  io:format(Fd, "[~s] Realizase o cambio de modo ~s a ~s~n", [Timestamp, "DIVISION", "DIVISION"]),
  {ok, Fd};


%%% Escritura de Operaciones
handle_event({operacion, suma, Num, Ac, Res}, Fd) ->
    Timestamp = format_utc_timestamp(),
    io:format(Fd, "[~s] Realizase a suma ~p + ~p con resultado ~p~n", [Timestamp, Num, Ac, Res]),
    {ok, Fd};
handle_event({operacion, resta, Num, Ac, Res}, Fd) ->
    Timestamp = format_utc_timestamp(),
    io:format(Fd, "[~s] Realizase a resta ~p - ~p con resultado ~p~n", [Timestamp, Num, Ac, Res]),
    {ok, Fd};
handle_event({operacion, producto, Num, Ac, Res}, Fd) ->
    Timestamp = format_utc_timestamp(),
    io:format(Fd, "[~s] Realizase o producto ~p * ~p con resultado ~p~n", [Timestamp, Num, Ac, Res]),
    {ok, Fd};
handle_event({operacion, division, Num, Ac, Res}, Fd) ->
    Timestamp = format_utc_timestamp(),
    io:format(Fd, "[~s] Realizase a division ~p / ~p con resultado ~p~n", [Timestamp, Num, Ac, Res]),
    {ok, Fd}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
format_utc_timestamp() ->
    TS = {_,_,Micro} = os:timestamp(),
    {{Year,Month,Day},{Hour,Minute,Second}} =	calendar:now_to_universal_time(TS),
    Mstr = element(Month,{"Xan","Feb","Mar","Abr","Mai","Xun","Xul","Ago","Set","Out","Nov","Dec"}),
    io_lib:format("~2w ~s ~4w ~2w:~2..0w:~2..0w.~6..0w",
      [Day,Mstr,Year,Hour,Minute,Second,Micro]).