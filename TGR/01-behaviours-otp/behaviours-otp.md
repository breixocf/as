##Proceso Calculadora (gen_fsm)
Implementa un **proceso calculadora (calculadora.erl)** empregando o **behaviour `gen_fsm`**. A súa interface pública debe ser:

+ **`Operación on/0`**, que arrancará a calculadora.
`-spec on() -> ok.`

+ **`Operación off/0`**, que deterá a calculadora.
`-spec off() -> ok.`

+ **`Operación acumular/1`**, que operará a cantidade proporcionada como argumento co valor acumulado na memoria da calculadora, segundo a operación coa que estea configurada.
`-spec acumular(N :: number()) -> number().`

+ **`Operación modo/1`**, que cambiará a calculadora de modo (operación). A calculadora debe soportar alomenos 4 modos: suma, resta, producto, division.
`-spec modo(O :: atom()) -> ok.`


##Proceso de Log (gen_event)
Implementa un **proceso de log (log_calculadora.erl)**, empregando o **behaviour `gen_event`**, que rexistre as operacións realizadas coa calculadora nun ficheiro log.txt.


##Proceso Supervisor Calculadora (supervisor)
Implementa un **supervisor da calculadora (supervisor_calculadora.erl)**, empregando o **behaviour `supervisor`**, que re-arranque a calculadora se se realiza algunha operación inválida (por exemplo, unha división por cero, ou unha invocación a modo/1 proporcionando un modo de operación non soportado).


Para completar esta tarefa, debes subir un **ficheiro tar/tgz/zip/rar/tar** que conteña o código fonte dos tres módulos anteriores, ademáis dun **diagrama de estados da calculadora** en formato `PNG`, `JPG` ou `PDF`.


O ficheiro tar/tgz/zip/rar/tar non deberá conter ficheiros binarios (.beam) nin documentación que poida ser xerada (.html, etc.).