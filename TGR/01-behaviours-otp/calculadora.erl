%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Breixo Camiña Fernández
%%% @copyright (C) 2015
%%% @doc
%%%
%%% @end
%%% Created : 16. Mar 2015 7:51 PM
%%%-------------------------------------------------------------------
-module(calculadora).
-author("BreixoCF").
-behaviour(gen_fsm).

%% Public API
-export([on/0, off/0, modo/1, acumular/1]).

%% Internal API (gen_fsm)
-export([init/1, handle_event/3, terminate/3]).

%% Internal API (estados)
-export([suma/2, suma/3, resta/2, resta/3, producto/2, producto/3, division/2, division/3]).

-define(CALC, calculadora).
-define(LOG_MODULE, log_calculadora).
-define(LOG_FILE, "log.txt").

%%%===================================================================
%%% Public API
%%%===================================================================
-spec on() -> ok.
on() ->
    gen_fsm:start_link({local, ?CALC}, ?CALC, 0, []).

-spec off() -> ok.
off() ->
    gen_fsm:send_all_state_event(?CALC, stop).

%%% En realidad, para estar perfecta, no debería capturar aquí los distintos modos, si no pasarle un átomo
%%% al gen_fsm y que el los filtrara.
-spec modo(O::atom())-> ok.
modo(suma) ->
    gen_fsm:send_event(?CALC, {modoSuma});
modo(resta) ->
    gen_fsm:send_event(?CALC, {modoResta});
modo(producto) ->
    gen_fsm:send_event(?CALC, {modoProducto});
modo(division) ->
    gen_fsm:send_event(?CALC, {modoDivision}).

% modo(O) ->
%    gen_fsm:send_all_state_event(?CALC, O).

-spec acumular(N::number()) -> number().
acumular(N) ->
    gen_fsm:sync_send_event(?CALC, {numero, N}).

%%%===================================================================
%%% Internal API (gen_fsm)
%%%===================================================================
init(Ac) ->
    {ok, _Pid} = gen_event:start_link({local, logcalc}),
    gen_event:add_handler(logcalc, ?LOG_MODULE, ?LOG_FILE),
    gen_event:notify(logcalc, {on}),
    {ok, suma, Ac}.

handle_event(stop, _Estado, _DatosEstado) ->
    {stop, normal, []}.

terminate(normal, _Estado, _DatosEstado) ->
    gen_event:notify(logcalc, {off}),
    gen_event:stop(logcalc),
    ok.

%%%===================================================================
%%% Internal API (estados)
%%%===================================================================
% Cambio Modo SUMA
suma({modoSuma}, Ac) ->
    gen_event:notify(logcalc, {cambio, suma, suma}),
    {next_state, suma, Ac};
suma({modoResta}, Ac) ->
    gen_event:notify(logcalc, {cambio, suma, resta}),
    {next_state, resta, Ac};
suma({modoProducto}, Ac) ->
    gen_event:notify(logcalc, {cambio, suma, producto}),
    {next_state, producto, Ac};
suma({modoDivision}, Ac) ->
    gen_event:notify(logcalc, {cambio, suma, division}),
    {next_state, division, Ac}.
% Cálculo Modo SUMA
suma({numero, N}, _From, Ac) ->
    gen_event:notify(logcalc, {operacion, suma, N, Ac, Ac+N}),
    {reply, Ac+N, suma, Ac+N}.

% Cambio Modo RESTA
resta({modoSuma}, Ac) ->
  gen_event:notify(logcalc, {cambio, resta, suma}),
  {next_state, suma, Ac};
resta({modoResta}, Ac) ->
  gen_event:notify(logcalc, {cambio, resta, resta}),
  {next_state, resta, Ac};
resta({modoProducto}, Ac) ->
  gen_event:notify(logcalc, {cambio, resta, producto}),
  {next_state, producto, Ac};
resta({modoDivision}, Ac) ->
  gen_event:notify(logcalc, {cambio, resta, division}),
  {next_state, division, Ac}.
% Cálculo Modo RESTA
resta({numero, N}, _From, Ac) ->
  gen_event:notify(logcalc, {operacion, resta, N, Ac, Ac-N}),
  {reply, Ac-N, resta, Ac-N}.

% Cambio Modo PRODUCTO
producto({modoSuma}, Ac) ->
  gen_event:notify(logcalc, {cambio, producto, suma}),
  {next_state, suma, Ac};
producto({modoResta}, Ac) ->
  gen_event:notify(logcalc, {cambio, producto, resta}),
  {next_state, resta, Ac};
producto({modoProducto}, Ac) ->
  gen_event:notify(logcalc, {cambio, producto, producto}),
  {next_state, producto, Ac};
producto({modoDivision}, Ac) ->
  gen_event:notify(logcalc, {cambio, producto, division}),
  {next_state, division, Ac}.
% Cálculo Modo PRODUCTO
producto({numero, N}, _From, Ac) ->
  gen_event:notify(logcalc, {operacion, producto, N, Ac, Ac*N}),
  {reply, Ac*N, producto, Ac*N}.

% Cambio Modo DIVISION
division({modoSuma}, Ac) ->
  gen_event:notify(logcalc, {cambio, division, suma}),
  {next_state, suma, Ac};
division({modoResta}, Ac) ->
  gen_event:notify(logcalc, {cambio, division, resta}),
  {next_state, resta, Ac};
division({modoProducto}, Ac) ->
  gen_event:notify(logcalc, {cambio, division, producto}),
  {next_state, producto, Ac};
division({modoDivision}, Ac) ->
  gen_event:notify(logcalc, {cambio, division, division}),
  {next_state, division, Ac}.
% Cálculo Modo DIVISION
division({numero, N}, _From, Ac) ->
  gen_event:notify(logcalc, {operacion, division, N, Ac, Ac/N}),
  {reply, Ac/N, division, Ac/N}.