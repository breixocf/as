%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Breixo Camiña Fernández
%%% @copyright (C) 2015
%%% @doc
%%%
%%% @end
%%% Created : 17. Mar 2015 8:26 PM
%%%-------------------------------------------------------------------
-module(supervisor_calculadora).
-author("BreixoCF").

-behaviour(supervisor).

%% API
-export([start/0]).

%% Supervisor callbacks
-export([init/1]).
-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

start() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([]) ->
  RestartStrategy = one_for_one,
  MaxRestarts = 1000,
  MaxSecondsBetweenRestarts = 1,
  Flags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},
  Restart = permanent,
  Shutdown = 2000,
  Type = worker,
  Calculadora = {calculadora, {calculadora, on, []},
  Restart, Shutdown, Type, [calculadora]},
  {ok, {Flags, [Calculadora]}}.