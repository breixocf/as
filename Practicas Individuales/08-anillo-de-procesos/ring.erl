%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 8 - Módulo Ring
%%%
%%% Módulo que debe crear ProcNum procesos conectados en forma de anillo y enviar MsgNum veces el mensaje Message al anillo, de forma que recorra el anillo completo.
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================
-module(ring).
-author("BreixoCF").
-export([start/3, init/3, loop/1]).

start(ProcNum, MsgNum, Message) ->
  Master = spawn(ring, init, [ProcNum, MsgNum, Message]),
  register(master, Master),
  ok.

init(1, MsgNum, Message) ->
  Master = whereis(master),
  Master ! {MsgNum-1, Message},
  loop(Master);
init(ProcNum, MsgNum, Message) ->
  Slave = spawn(ring, init, [ProcNum-1, MsgNum, Message]),
  loop(Slave).

loop(Pid) ->
  receive
    {0, _Message} ->
      %io:format("(~p) PARA EL MAESTRO Y FINALIZA EJECUCIÓN! ~n", [Pid]),
      Pid ! stop,
      unregister(master);
    {MsgNum, Message} ->
      %io:format("(~p): ENVIA ~p A (~p) ~n", [self(), Message, Pid]),
      Pid ! {MsgNum-1, Message},
      loop(Pid);
    stop ->
      %io:format("(~p) PARA Y MANDA PARAR A (~p) ~n", [self(), Pid]),
      Pid ! stop;
    _ ->
      loop(Pid)
  end.