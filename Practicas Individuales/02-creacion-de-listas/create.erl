%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 2 - Módulo Creación de listas
%%%
%%% Este módulo implementa las funciones Create y Reverse_create
%%% para crear listas de manera sencilla.
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================

-module(create).
-export([create/1, reverse_create/1]).

%%% ----- CREATE -----
create(0) -> [];
create(N) when N > 0 ->	create(N-1) ++ [N].
%%% ------------------


%%% ----- REVERSE_CREATE -----
reverse_create(0) -> [];
reverse_create(N) when N > 0 -> [N] ++ reverse_create(N-1).
% También valdría:
% reverse_create(N) when N > 0 -> [N|reverse_create(N-1)].
%%% --------------------------