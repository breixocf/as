%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 6 - Módulo de Almacenamiento Clave-Valor
%%%
%%% Módulo de almacenamiento clave-valor basado en listas.
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================
-module(db).
-author("BreixoCF").

%% API
-export([new/0, write/3, delete/2, read/2, match/2, destroy/1, reverse/1]).

% Función Auxiliar (módulo manipulating)
reverse(List) -> reverse(List, []).
reverse([H|T], List2) -> reverse(T, [H|List2]);
reverse([], List2) -> List2.


new() ->[].

write(Key, Element, DbRef) -> [{Key,Element}|DbRef].

delete(Key, DbRef) -> delete(Key, DbRef, []).

delete(_Key, [], NewDbRef) -> reverse(NewDbRef);
delete(Key, [{Key, _Element}|T], NewDbRef) -> delete(Key, T, NewDbRef);
delete(Key, [H|T], NewDbRef) -> delete(Key, T, [H|NewDbRef]).

read(_Key, []) -> {error, instance};
read(Key, [{Key, Element}|_T]) -> {ok, Element};
read(Key, [{_Key, _Element}|T]) -> read(Key, T).

match(Element, DbRef) -> match(Element, DbRef, []).

match(_Element, [], ListElement) -> reverse(ListElement);
match(Element, [{Key, Element}|T], ListElement) -> match(Element, T, [Key|ListElement]);
match(Element, [_H|T], ListElement) -> match(Element, T, ListElement).

destroy(_DbRef) -> ok.