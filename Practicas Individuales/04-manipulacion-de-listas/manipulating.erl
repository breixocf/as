%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 4 - Módulo Manipulación de Listas
%%%
%%% Este módulo implementa las funciones Filter, Reverse, Concatenate y
%%% Flatten similares a las del Módulo Lists.
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================

-module(manipulating).
-export([filter/2, reverse/1, concatenate/1, flatten/1]).

%%% ----- FILTER -----
filter([], _) -> [];

filter([H|T], N) -> 
    if
        H =< N	-> [H | filter(T, N)];
        true 	-> filter(T, N)
	end.
%%% ------------------


%%% ----- REVERSE -----
reverse(List) -> reverse(List, []).
reverse([], Acc) -> Acc;
reverse([H|T], Acc) -> reverse(T, [H|Acc]).

%% NO TAIL-RECURSION
%% reverse([]) -> [];
%% reverse([H|T]) -> reverse(T) ++ H.

%%% -------------------


%%% ----- CONCATENATE -----
concatenate(List) -> concatenate(List, []).

concatenate([[H|T]|List], Aux) -> concatenate([T|List], [H|Aux]);
concatenate([[]|List], Aux) -> concatenate(List, Aux);
concatenate(List, Aux)-> reverse(Aux).
%%% -----------------------


%%% ----- FLATTEN -----
flatten(List) -> reverse(flatten(List, [])).

flatten([], Aux) -> Aux;
flatten([H|T], Aux) when is_list(H) -> flatten(T, flatten(H, Aux));
flatten([H|T], Aux) -> flatten(T, [H|Aux]).
%%% -------------------