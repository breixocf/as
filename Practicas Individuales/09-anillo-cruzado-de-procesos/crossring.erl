%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 9 - Módulo Crossring
%%%
%%%
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================
-module(crossring).
-author("breixo").

%% API
-export([]).
