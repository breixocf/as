%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 3 - Módulo Efectos Colaterales
%%%
%%% Este módulo implementa las funciones Print y Even_print,
%%% que imprime una lista de valores de 1 a N y una lista
%%% de los pares de 1 a N.
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================

-module(effects).
-export([print/1, even_print/1]).

%%% ----- PRINT -----
print(N) ->
	List=lists:seq(1, N),
	io:format("~w~n", [List]).
%%% -----------------


%%% ----- EVEN_PRINT -----
even_print (N)->
	List=lists:seq(2, N, 2),
	io:format("~w~n", [List]).
%%% ----------------------