%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 7 - Módulo Servidor Echo
%%%
%%% Módulo servidor que dependiendo del mensaje recibido deberá imprimir el mensaje o detener su ejecución.
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================
-module(echo).
-author("BreixoCF").

%% API
-export([start/0, stop/0, print/1, loop/0]).
start() -> register(echo, spawn(echo, loop, [])),
           ok.

stop() -> echo ! stop,
          unregister(echo),
          ok.

print(Term) -> echo ! {print, Term},
               ok.

loop() -> receive
              stop -> {ok, self()};
              {print, Term} -> io:format("~p~n", [Term]), loop();
              _ -> loop(), ok
          end.
