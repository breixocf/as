%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 1 - Módulo Boolean simple
%%%
%%% Este módulo implementa las funciones lógicas Not, And y Or.
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================

-module(boolean).
-export([b_not/1, b_and/2, b_or/2]).

%%% ----- B_NOT -----
b_not(true) -> false;
b_not(false) -> true.
%%% -----------------


%%% ----- B_AND -----
b_and(true, true) -> true;
% b_and(_, _) -> false;
b_and(A, B) when is_atom(A), is_atom(B) -> false.
%%% -----------------


%%% ----- B_OR -----
b_or(false, false) -> false;
% b_or(_, _) -> true;
b_or(A, B) when is_atom(A), is_atom(B) -> true.
%%% ----------------