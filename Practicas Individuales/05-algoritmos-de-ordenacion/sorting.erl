%%% -*- coding: utf-8 -*-
%%% ========================================================================
%%% @author Breixo Camiña Fernández <breixo.camina@udc.es>
%%% @copyright (C) 2015, Breixo Camiña
%%% @doc Práctica 5 - Módulo de Ordenación
%%%
%%% Módulo de ordenación que incluye los algoritmos Quicksort y Mergesort.
%%% Breixo Camiña Fernández
%%% Arquitectura Software		   2014/2015
%%% Grupo 1.3.1
%%% @end
%%% ========================================================================

-module(sorting).
-export([quicksort/1, mergesort/1]).

%%% ----- QUICKSORT -----
quicksort([Pivote|T]) ->
    quicksort([ X || X <- T, X < Pivote]) ++
    [Pivote] ++
    quicksort([ X || X <- T, X >= Pivote]);
quicksort([]) -> [].
%%% ---------------------

%%% ----- MERGESORT -----
merge(L, [])  -> L;
merge([], R)  -> R;
merge([L|Left], [R|Right])  ->
    if
        L < R   -> [L | merge(Left, [R|Right])];
        % Se podría poner true, pero para facilitar la lectura
        % del código (P52 del libro de Fred Hebert).
        L >= R  -> [R | merge([L|Left], Right)]
    end.

mergesort([])   -> [];
mergesort([H])  -> [H];
mergesort(List) ->
    {L, R} = lists:split(length(List) div 2, List),
    merge(mergesort(L), mergesort(R)).
%%% ---------------------