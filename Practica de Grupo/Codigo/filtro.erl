%%%-------------------------------------------------------------------
%%% @author grupo AS
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------

-module(filtro).
-author("grupo AS").
%% API
-export([filterBadWords/2, replace/3]).

filterBadWords(Msg , BadWords) ->
	Trozos = string:tokens(string:to_lower(Msg), "¿?¡!,.;<>#:'"" "),
	replace( Trozos, BadWords, [] ).

%Funcion para reconstruir los signos de puntuacion. No se usa de momento xk contiene fallos que solucionar
%rebuild( New, _, []) -> New;
%rebuild( New, Original, [Char|Chars])->
%	case string:str(Original, Char ) of
%		0 -> rebuild (New, Original, Chars);		
%		Pos -> 
%			P = string:concat( string:concat( string:sub_string(New,1, Pos), Char ), " " ),
%			P2 = string:concat ( P, string:sub_string(New, Pos) ), 
%			rebuild( P2, Original, Chars )
%	end.


replace ( Results, [] , _Aux_Msg ) ->
	string:join(Results," ");
replace ( [], [_Word|BadWords] , Aux_Msg ) ->
	replace ( Aux_Msg, BadWords, [] );
replace ( [Word|Msg], [Word|BadWords] , Aux_Msg ) ->
	Tam = string:len(Word),
	replace(Msg, [Word|BadWords], [string:concat(string:concat(string:sub_string(Word,1,1), string:copies("*", string:len(Word)-2)), string:sub_string(Word, Tam, Tam) )|Aux_Msg]);
replace([NoMatch|Msg], [Word|BadWords] , Aux_Msg) ->
	replace(Msg, [Word|BadWords], [NoMatch|Aux_Msg]).


