-module(filtro_tests).

-include_lib("eunit/include/eunit.hrl").


no_word_test() ->
	Msg =  "bienvenido a guachap",
	Msg2 = "bienvenido a guachap",
	Bad_words= ["pene","vagina"],
	Msg2 = filtro:filterBadWords(Msg, Bad_words).

one_word_test()->
	Msg =  "bienvenido pene a guachap",
	Msg2 = "bienvenido p**e a guachap",
	Bad_words= ["pene","vagina"],
	Msg2 = filtro:filterBadWords(Msg, Bad_words).

two_different_words_test()->
	Msg =  "vagina bienvenido pene a guachap",
	Msg2 = "v****a bienvenido p**e a guachap",
	Bad_words= ["pene","vagina"],
	Msg2 = filtro:filterBadWords(Msg, Bad_words).

two_same_words_test()->
	Msg =  "pene bienvenido pene a guachap",
	Msg2 = "p**e bienvenido p**e a guachap",
	Bad_words= ["pene","vagina"],
	Msg2 = filtro:filterBadWords(Msg, Bad_words).


