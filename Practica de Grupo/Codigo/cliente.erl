%%%-------------------------------------------------------------------
%%% @author Breixo Camiña y David Torres
%%% @copyright (C) 2015, Grupo 1.3
%%% @doc
%%%
%%% @end
%%% Created : 23. Apr 2015 12:44 PM
%%%-------------------------------------------------------------------

-module(cliente).
-author("grupo AS").
%% API
-export([start/1, send/2, buzon/2, logout/0]).
-define(PID_BUZON, pid_buzon).

%% IMPORT CIPHER MODULE
-import(cipher, [encode/1, decode/1]).

server_node () ->
	servidor@david. %Nombre del nodo donde esta el servidor.

start(User) ->
      PidServer = whereis(?PID_BUZON),
      case(PidServer) of
        undefined ->
		register(?PID_BUZON, spawn(cliente, buzon, [server_node(), User]));
	_PidServer ->
		io:format("Ya te encuentras registrado~n")
      end.

buzon(Server_node, User)->
	{servidor, Server_node} ! {registrar, User, self() },
	receive
		{stop, Why} -> %Fallo en el registro
			io:format("~p~n", [Why]),
			exit(normal);
		{ok, exito} ->
			io:format("Te has registrado correctamente~n"),
			buzon(Server_node)
	end.

buzon(Server_node)->
	receive
		{logout} ->
			{servidor, Server_node} ! {logout, self()},
			cerrar_sesion(Server_node);
		{FromName, Msg} ->
			io:format("~p:~n  ~p~n",[FromName, Msg]),
			buzon(Server_node);
		{enviar, Dest, Message} ->
			{servidor, Server_node} ! {envio, self(), Dest, Message},
			buzon(Server_node);
		{recibir, From, Msg} ->
			io:format("~p:~n  ~p~n", [From, cipher:decode(Msg)]),
			buzon(Server_node)
	end.

cerrar_sesion(_Server_node) ->
	receive
		{ok} -> io:format("Te has deslogueado correctamente, que tengas un buen dia~n~n"),
		exit(normal)
	end.


send(Dest, Message) ->
      case whereis(?PID_BUZON) of
        undefined ->
			io:format("El mensaje no se ha podido enviar, servicio no disponible temporalmente.~n");
        PidServer ->
      		PidServer ! {enviar, Dest, cipher:encode(Message)},
		enviado
      end.

logout() ->
	case whereis(?PID_BUZON) of
		undefined ->
			io:format("No te encuentras actualmente logueado~n");
		PidBuzon -> PidBuzon ! {logout}
	end.
