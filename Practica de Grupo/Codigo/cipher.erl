%%%-------------------------------------------------------------------
%%% @author Alejandro Martín
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 9. May 2015 4:44 PM
%%%-------------------------------------------------------------------

%%
%% Función de cifrado:  io:format("~s~n", [lists:map(fun(C) -> C+1 end, "HOLA")]).
%%

-module(cipher).
%% API
-export([encode/1, decode/1]).

encode(Msg) ->
%%    io:format("~s~n", [lists:map(fun(C) -> C+1 end, Msg)]).
    lists:map(fun(C) -> C+1 end, Msg).

decode(Msg) ->
%%    io:format("~s~n", [lists:map(fun(C) -> C-1 end, Msg)]).
    lists:map(fun(C) -> C-1 end, Msg).
