%%%-------------------------------------------------------------------
%%% @author Breixo Camiña
%%% @copyright (C) 2015, Grupo 1.3
%%% @doc
%%%
%%% @end
%%% Created : 30. Apr 2015 11:57 AM
%%%-------------------------------------------------------------------
-module(servidor).
-author("grupo AS").
%% API
-export([start/0, loop/2, buscarUser/2]).
-define(SERVER_NAME, servidor).

start() -> 
	BadWordsList = ["pene", "vagina", "gilipollas", "imbecil"],
	register(servidor, spawn(servidor, loop, [[], BadWordsList]) ).

loop(ListaUsuarios, BadWordsList) ->
          receive
            {registrar, User, Pid} -> 
		loop (registroUser(Pid, User, ListaUsuarios), BadWordsList);
            {logout, FromPid} -> 
		FromPid ! {ok},
		loop(borrarUser(ListaUsuarios, {buscarUser(ListaUsuarios, FromPid),FromPid}), BadWordsList);
            {envio, PidOrigen, Dest, Message} ->
              case buscarPid(ListaUsuarios, Dest) of
                  -1 -> 
			PidOrigen ! {notfound},
			loop(ListaUsuarios, BadWordsList);
                  PidDest -> 
			PidDest ! {recibir, buscarUser(ListaUsuarios, PidOrigen), cipher:encode(filtro:filterBadWords(cipher:decode(Message), BadWordsList ) )},
		 	io:format("Mensaje recibido de ~p, reenviando a ~p~n",[PidOrigen,Dest]),
		  	loop(ListaUsuarios, BadWordsList)	
              end
          end.
          

registroUser(From, User, ListaUsuarios)->
	case lists:keymember(User, 1, ListaUsuarios) of
		true -> 
			From ! {stop, usuario_ya_registrado},
			ListaUsuarios;
		false ->
			io:format("Usuario ~p [~p] registrado en el servidor~n", [User, From]),
			From ! {ok, exito},
			[{User, From} | ListaUsuarios]
	end.


buscarUser([{User, Pid}|_], Pid) -> User;
buscarUser([{_User, _Pid}|T], Pid) -> buscarUser(T, Pid);
buscarUser([], _Pid) -> io:format("Usuario no registrado"), -1.

buscarPid([{User, Pid}|_], User) -> Pid;
buscarPid([{_User, _Pid}|T], User) -> buscarPid(T, User);
buscarPid([], _User) -> io:format("Usuario no registrado"), -1.

borrarUser(ListaUsuarios, User) ->
	lists:delete(User, ListaUsuarios).
